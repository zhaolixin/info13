from flask import session,render_template,current_app



from . import news_blue

# 使用模板加载项目首页
@news_blue.route('/')
def index():
    session['user'] = '2018'
    return render_template('news/index.html')

# 项目logo图标加载
@news_blue.route('/favicon.ico')
def favicon():
    """
    浏览器不是每次请求都加载logo图标，实现代码后，
    1、清除浏览器的浏览信息和缓存。
    2、让浏览器彻底退出重新启动。
    3、重新打开浏览器会有logo图标。
    :return:
    """
    # 通过current_app调用Flask框架内部的发送静态文件给浏览器的函数send_static_file
    return current_app.send_static_file('news/favicon.ico')
